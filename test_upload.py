import pytest
import upload
import boto3
from moto import mock_s3

@mock_s3
def test_upload():
    conn = boto3.client('s3', region_name='us-east-1')
    bucket = 'comp630-m1-f19'
    # We need to create the bucket since this is all in Moto's 'virtual' AWS account
    conn.create_bucket(Bucket=bucket)
    upload.upload_icecream(
        "test.boto", 
        bucket, 
        "proftim.boto")

    assert True